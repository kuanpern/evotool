import random
import heapq
import numpy as np
import logging

def FitnessProportionateSelection(chromosomes, n):
	p = [chromosome.fitness for chromosome in chromosomes]
	assert min(p) >= 0, 'fitness score must be >= 0'
	p = np.array(p) / np.sum(p) # normalization
	return np.random.choice(chromosomes, n, p)
# end def

def StochasticUniversalSampling(chromosomes, n):
	def RWS(chromosomes, points):
		keep = []
		for p in points:
			i = 0
			while True:
				s = sum([chromosome.fitness for chromosome in chromosomes[:i]])
				if s < p:
					i += 1
				else:
					break
				# end if
			# end while
			keep.append(chromosomes[i-1])
		# end for
		return keep
	# end def

	F = sum([chromosome.fitness for chromosome in chromosomes])
	P = F / n
	start = random.random()*P # random.randint(0, int(round(P)))
	pointers = [start + i*P for i in range(0, n-1)]
	return RWS(chromosomes, pointers)
# end def

def TournamentSelection(chromosomes, n, q=0.2, p=1):
	# q: % of population in tournament
	# p: probability of selecting one chromosome
	
	# select into tournament
	z = int(len(chromosomes) * q)
	
	selecteds = []
	while True:
		candidates = random.choices(chromosomes, k=z)
		# select winners with probabilities
		sorter = [(-chromosome.fitness, chromosome) for chromosome in chromosomes]
		sorter.sort()
		index = None
		for i in range(len(sorter)):
			if random.random() < p*(1-p)**i:
				index = i
				break
			# end if
		# end for
		if index is not None:
			selecteds.append(chromosomes[index])
		# end if
	# end while
	if len(selecteds) >= n:
		return selecteds[:n]
	# end if
# end def

def crossover(chromosome_i, chromosome_j):
	crossover_pt = random.randint(0, len(chromosome_i.sequence) - 1)
	seq_p = chromosome_i.sequence[:crossover_pt] + chromosome_j.sequence[crossover_pt:]
	seq_q = chromosome_j.sequence[:crossover_pt] + chromosome_i.sequence[crossover_pt:]
	return [Chromosome(sequence=seq_p), Chromosome(sequence=seq_q)]
# end def

def mutate(chromosome, mutation_rate, codons=None):
	for i in range(len(chromosome)):
		if random.random() <= mutation_rate:
			chromosome.sequence[i] = random.choice(codons) # randomly select from possible codons
		# end if
	# end for
	return chromosome
# end def

class Chromosome:
	def __init__(self, sequence=[]):
		self.fitness = np.nan
		self.sequence = sequence
	# end def
	
	def __len__(self):
		return len(self.sequence)
	# end def
# end class

class ESenv:
	def __init__(self, pool, codons, pool_size=100, logger=logging.getLogger()):
		self.iteration_n = 0
		self.mutation_rate = 0.01
		self.crossover_rate = 0.8
		self.n_elites = 0 # turn elitism off by default
		self.pool_size = pool_size
		self.selection_strategy = 'StochasticUniversalSampling'
		
		# read initial pool and codons
		self.pool   = pool
		self.codons = codons
		self.logger = logger
		
		self._update_internals()
	# end def
	
	def set_params(self, params):
		self.__dict__.update(params) # ! note: not secure
		self._update_internals()
	# end def
	
	def _update_internals(self):
		# update selection method
		if self.selection_strategy == 'StochasticUniversalSampling':
			self.select_chromosomes = StochasticUniversalSampling
		elif self.selection_strategy == 'FitnessProportionateSelection':
			self.select_chromosomes = FitnessProportionateSelection
		elif self.selection_strategy == 'TournamentSelection':
			self.select_chromosomes = TournamentSelection
		# end if
	# end def

	def select_elites(self, n):
		sorter = [(chromosome.fitness, chromosome) for chromosome in self.pool]
		selecteds = heapq.nlargest(n, sorter)
		elites = [_[1] for _ in selecteds]
		return elites
	# end def
		
	def select_chromosomes(self):
		pass
	# end def

	def mutate_pool(self):
		for i in range(len(self.pool)):
			self.pool[i] = mutate(self.pool[i], mutation_rate=self.mutation_rate, codons=self.codons)
		# end for
	# end def

	def crossover_pool(self):
		n_events = int(len(self.pool) * self.crossover_rate)
		for k in range(n_events):
			i = random.randint(0, len(self.pool)-1)
			j = random.randint(0, len(self.pool)-1)
			chromosome_i, chromosome_j = self.pool[i], self.pool[j]
			chromosome_p, chromosome_q = crossover(chromosome_i, chromosome_j)
			self.pool[i], self.pool[j] = chromosome_p, chromosome_q
		# end for
		return self.pool
	# end def
	
	def export_chromosomes(self):
		return
	# end def

	def decode_chromosome(self, chromosome):
		raise NotImplementedError('"decode_chromosome" method not implemented')
	# end def
	
	def score(self, decoded):
		raise NotImplementedError('"score" method not implemented')
	# end def
	
	def det_converged(self, new_pool, old_pool):
		raise NotImplementedError('"det_converged" method not implemented')
	# end def
	
	def compute_homogeneity(self):
		homogeneity = len(set([tuple(x) for x in survivors])) / (len(survivors) + 0.0) # homogeneity of population
	# end def
	
	def report(self):
		self.logger.info('Best chromosome in the generation: %s' % (self.decode(list(survivors)[0]),))
		self.logger.info('Convergence: %4.1f%%' % (homogeneity*100,))
	# end def
	
	def run(self):
		self._update_internals()
		
		while True:
			self.iteration_n += 1
			self.logger.info('Generation: %d' % (self.iteration_n,))

			# create a new pool
			new_pool = []

			# compute fitness value
			for chromosome in self.pool:
				decoded = self.decode_chromosome(chromosome)
				chromosome.fitness = self.score(decoded)
			# end for

			# export results
			self.export_chromosomes()

			# select elites
			elites = self.select_elites(self.n_elites)
			new_pool.extend(elites)

			# selection
			n = self.pool_size - len(elites)
			selecteds = self.select_chromosomes(self.pool, n)
			new_pool.extend(selecteds)

			# convergence check
			if self.det_converged(new_pool, self.pool):
				break
			else:
				self.pool = new_pool
			# end if

			# crossover and mutate
			self.crossover_pool()
			self.mutate_pool()
		# end while		
	# end def
# end class
