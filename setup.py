import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()
# end with

setuptools.setup(
    name="evotool",
    version="0.1.0",
    author="Tan Kuan Pern",
    author_email="kptan86@gmail.com",
    description="Collection of evolutionary computation tools and modules",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/kuanpern/evotool",
    packages=setuptools.find_packages(),
    install_requires=[
      'numpy>=1.17',
    ]
)
